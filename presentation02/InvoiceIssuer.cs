public void SaveInvoiceButton_Clicked(InvoiceModel invoice)
{
    _dbContext.Invoices.Add(invoice);
    _dbContext.SaveChanges();
}

public void SaveInvoiceButton_Clicked(InvoiceModel invoice)
{
    try
    {
        _logger.Log("Started issuing invoice:");

        _view.SaveInvoiceButton.Enabled = false;
        
        _dbContext.Database.BeginTransaction();

        var errors = new List<string>();
        if(invoice.TotalPrice <= 0)
            errors.Add("TotalPrice must be greater than zero.");
        if(invoice.Customer is null)
            errors.Add("Customer is required");
        // and other model validations ...

        if(errors.Any())
        {
            _view.statusTextBox.Text = string.Join("\r\n", errors);
            _logger.Log("Error in model");
            transaction.Rollback();
            return;
        }

        var customerCreditRemaining =
            _dbContext.Parties.Find(Invoice.CustomerRef).Credit
            - _dbContext.Invoices.Where(i => !i.IsSetteled).Sum(i => i.TotalPrice);

        if(customerCreditRemaining < invoice.TotalPrice)
        {
            _view.statusTextBox.Text = "The customer does not have enought credit.";
            _logger.Log("The customer does not have enought credit");
            transaction.Rollback();
            return;
        }
        
        if(invoice.Number is null)
        {
            invoice.Number = _dbContext.Invoices.Max(i => i.Number) + 1;
        }
        else if(invoice.Number <= _dbContext.Invoices.Max(i => i.Number))
        {
            _view.statusTextBox.Text = "Invalid invoice number.";
            _logger.Log("Invalid invoice number");
            transaction.Rollback();
            return;
        }

        _dbContext.Invoices.Add(invoice);
        _dbContext.SaveChanges();

        _dbContext.Database.CommitTransaction();

        statusTextBox.Text = $"Done";

        _smsSender.Send(invoice.Customer.PhoneNumber, @$"Dear {invoice.Customer.FullName}
Your invoice for {invoice.TotalPrice} is due by {invoice.DueDate}. Please click https://shalghamkaran.com/pay/{invoice.Number} to pay.
Shalgham karan co.");

    }
    catch(Exception e)
    {
        _dbContext.Database.RollbackTransaction();
        statusTextBox.Text = $"Error";
        _logger.Log($"Error: {e.Message}");
    }
    finally
    {
        _view.SaveInvoiceButton.Enabled = true;
        _logger.Log($"Issuing invoice finished.");
    }
}